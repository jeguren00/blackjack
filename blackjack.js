//https://teamtreehouse.com/library/create-an-array-of-objects

//Aquest sistema de llargues cadenes d'objectes emmagatzemades, i pereparades per agafar cualsevol , ha sigut creat i perfecionat
//Jordi Garcia Eguren.

//Aquest Joc de blackjack ha sigut creat per Jordi Garcia Egruen

//Aquest array emmagatzema totes les cartes, les imatges lligades a ella i el seu valor.
let cartes = [ {archivo:"./PNG/1.png",valor:"2",aleatorio:0},{archivo:"./PNG/2.png",valor:"2",aleatorio:1},
{archivo:"./PNG/3.png",valor:"2",aleatorio:2},{archivo:"./PNG/4.png",valor:"2",aleatorio:3},{archivo:"./PNG/5.png",valor:"3",aleatorio:4},
{archivo:"./PNG/6.png",valor:"3",aleatorio:5},{archivo:"./PNG/6.png",valor:"3",aleatorio:6},{archivo:"./PNG/7.png",valor:"3",aleatorio:7},
{archivo:"./PNG/8.png",valor:"3",aleatorio:8},{archivo:"./PNG/9.png",valor:"4",aleatorio:9},{archivo:"./PNG/10.png",valor:"4",aleatorio:10},
{archivo:"./PNG/11.png",valor:"4",aleatorio:11},{archivo:"./PNG/12.png",valor:"4",aleatorio:12},{archivo:"./PNG/13.png",valor:"5",aleatorio:13},
{archivo:"./PNG/14.png",valor:"5",aleatorio:14},{archivo:"./PNG/16.png",valor:"5",aleatorio:15},{archivo:"./PNG/17.png",valor:"6",aleatorio:16},
{archivo:"./PNG/18.png",valor:"6",aleatorio:17},{archivo:"./PNG/19.png",valor:"6",aleatorio:18},{archivo:"./PNG/20.png",valor:"6",aleatorio:19},
{archivo:"./PNG/21.png",valor:"7",aleatorio:20},{archivo:"./PNG/22.png",valor:"7",aleatorio:21},{archivo:"./PNG/23.png",valor:"7",aleatorio:22},
{archivo:"./PNG/24.png",valor:"7",aleatorio:23},{archivo:"./PNG/25.png",valor:"8",aleatorio:24},{archivo:"./PNG/26.png",valor:"8",aleatorio:25},
{archivo:"./PNG/27.png",valor:"8",aleatorio:26},{archivo:"./PNG/28.png",valor:"8",aleatorio:27},{archivo:"./PNG/29.png",valor:"9",aleatorio:28},
{archivo:"./PNG/30.png",valor:"9",aleatorio:29},{archivo:"./PNG/31.png",valor:"9",aleatorio:30},{archivo:"./PNG/32.png",valor:"9",aleatorio:31},
{archivo:"./PNG/33.png",valor:"10",aleatorio:32},{archivo:"./PNG/34.png",valor:"10",aleatorio:33},{archivo:"./PNG/35.png",valor:"10",aleatorio:34},
{archivo:"./PNG/36.png",valor:"10",aleatorio:35},{archivo:"./PNG/37.png",valor:"10",aleatorio:36},{archivo:"./PNG/38.png",valor:"10",aleatorio:37},
{archivo:"./PNG/39.png",valor:"10",aleatorio:38},{archivo:"./PNG/40.png",valor:"10",aleatorio:39},{archivo:"./PNG/41.png",valor:"10",aleatorio:40},
{archivo:"./PNG/42.png",valor:"10",aleatorio:41},{archivo:"./PNG/43.png",valor:"10",aleatorio:42},{archivo:"./PNG/44.png",valor:"10",aleatorio:43},
{archivo:"./PNG/45.png",valor:"10",aleatorio:44},{archivo:"./PNG/46.png",valor:"10",aleatorio:45},{archivo:"./PNG/47.png",valor:"10",aleatorio:46},
{archivo:"./PNG/48.png",valor:"10",aleatorio:47},{archivo:"./PNG/49.png",valor:"11",aleatorio:48},{archivo:"./PNG/50.png",valor:"11",aleatorio:49},
{archivo:"./PNG/51.png",valor:"11",aleatorio:50},{archivo:"./PNG/52.png",valor:"11",aleatorio:51},{archivo:"./PNG/red_back.png",valor:"0"}];

var money = 500;
var money = parseInt(money);
document.getElementById("ymon").innerHTML = money;

function clearGame() {
    document.getElementById("player").innerHTML = '';
    document.getElementById("enemigo").innerHTML = '';
    document.getElementById("esumplay").innerHTML= '';
    document.getElementById("sumplay").innerHTML= '';
}

var suma = 0;
var esuma = 0;

//Aquesta funció s'encarrega de la part del joc del jugador humá.
function player(suma) {
    //aquest for, repetiti per tot el codi recull el valor i l'arxiu d'una carta aleatoria.
    for (let i=0; i<2; i+=1) {
        var rnd = Math.floor(Math.random() * 51);
        var file = cartes[rnd].archivo;
        var val = cartes[rnd].valor;
        
        //aquesta part, repetida per tot el codi, crea la imatge de la carta corresponent.
        var x = document.createElement("IMG");
        x.setAttribute("src", file);
        x.setAttribute("width", "175");
        x.setAttribute("height", "230");
        x.setAttribute("alt", "The Card");
        document.getElementById("player").appendChild(x);
        
        //suma dels valors de les cartes.
        var suma = parseInt(suma) + parseInt(val);
        document.getElementById("sumplay").innerHTML = suma;  
        if (suma >= 21) {
            break;
        }    
    }
    
    //a partir de les dos cartes, min per guanyar, el client escull quantes vegades vol cartes.
    while (confirm("Quiere otra carta compadre?")) {
        var rnd = Math.floor(Math.random() * 51);
        var file = cartes[rnd].archivo;
        var val = cartes[rnd].valor;

        var x = document.createElement("IMG");
        x.setAttribute("src", file);
        x.setAttribute("width", "175");
        x.setAttribute("height", "230");
        x.setAttribute("alt", "The Card");
        document.getElementById("player").appendChild(x);

        var suma = parseInt(suma) + parseInt(val);
        document.getElementById("sumplay").innerHTML = suma;
        if (suma >= 21) {
            break;
        }
    }  
    console.log("suma1",suma);
}

//funcio de el jugador IA
function enemy(esuma) {
    var val = 0;
    for (let i=1; i<2; i+=1) {
        var rnd = Math.floor(Math.random() * 51);
        var file = cartes[rnd].archivo;
        var val = cartes[rnd].valor;

        var x = document.createElement("IMG");
        x.setAttribute("src", file);
        x.setAttribute("width", "175");
        x.setAttribute("height", "230");
        x.setAttribute("alt", "The Card");
        document.getElementById("enemigo").appendChild(x);
        
        var esuma = parseInt(esuma) + parseInt(val);
        document.getElementById("esumplay").innerHTML = esuma;
        if (esuma >= 21) {
            break;
        }      
    }
    
    //les normes del bj per al enemigo diuen que a partir de 16 no pot demanar mes cartes
    while(esuma <= 16){
        var rnd = Math.floor(Math.random() * 51);
        var file = cartes[52].archivo;
        var val = cartes[rnd].valor;

        var x = document.createElement("IMG");
        x.setAttribute("src", file);
        x.setAttribute("width", "175");
        x.setAttribute("height", "230");
        x.setAttribute("alt", "The Card");
        document.getElementById("enemigo").appendChild(x);
        
        var esuma = parseInt(esuma) + parseInt(val);
        document.getElementById("esumplay").innerHTML = esuma;
        if (esuma >= 21) {
            break;
        }
    }
    console.log("esuma1",esuma);

}
//control de les apostes del player, comença amb 500€
function apuestas() {
    var suma = document.getElementById("sumplay").innerHTML;
    var suma =  parseInt(suma);
    var esuma = document.getElementById("esumplay").innerHTML;
    var esuma =  parseInt(esuma);
    
    var money = document.getElementById("ymon").innerHTML;
    var money = parseInt(money);
    var bet = document.getElementById("bet").value;
    console.log("bet",bet);
    console.log("suma",suma);
    console.log("esuma",esuma);

    if (suma > 21) {
        alert("Has perdido");
        var money = parseInt(money) - parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;

    }
    else if(esuma > 21){
        alert("Has ganado");
        var money = parseInt(money) + parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;
    }
    if(suma == 21 && esuma == 21 ){
        alert("Has perdido, en caso de empate la casa gana pringao");
        var money = parseInt(money) - parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;
    }
    if (suma == 21) {
        alert("Has ganado");       
        var money = parseInt(money) + parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;  
                        
    } else if (esuma == 21) {
        alert("Has perdido");       
        var money = parseInt(money) - parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;
        
    }
    if(suma > esuma && suma < 21 && esuma < 21) {
        alert("Has ganado");       
        var money = parseInt(money) + parseInt(bet);  
        document.getElementById("ymon").innerHTML = money;
        return;
        
    } 
    if(suma < esuma && esuma <= 21 && suma > 21) {
        alert("Has perdido");       
        var money = parseInt(money) - parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;
        
    }
    if (suma > esuma && suma < 21) {
        alert("Has ganado");       
        var money = parseInt(money) + parseInt(bet);  
        document.getElementById("ymon").innerHTML = money;
        return;
    }
    if (suma < esuma && esuma < 21) {
        alert("Has perdido");       
        var money = parseInt(money) - parseInt(bet);
        document.getElementById("ymon").innerHTML = money;
        return;
    }
}

/*function hola() {
    var suma = document.getElementById("sumplay").innerHTML;
    var esuma = document.getElementById("esumplay").innerHTML;
    console.log("suma10",suma);
    console.log("esuma10",esuma);

}*/

function blackjackMano() {
    player(suma);
    enemy(esuma);
    apuestas();
    clearGame();      
}       

//alert(cartes[rnd].aleatorio);